import React from 'react'
import Router from 'core/Router'

function App() {
  return (
    <div className="App">
      <Router />
    </div>
  )
}

export default App
