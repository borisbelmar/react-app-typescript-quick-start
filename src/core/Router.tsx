import React, { Suspense } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import pages from 'components/pages'
import { DefaultLayout } from 'components/layout'

const SuspensePage = () => {
  return (
    <p>Loading lazy component</p>
  )
}

const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <Suspense fallback={<SuspensePage />}>
        <DefaultLayout>
          <Switch>
            {pages.map(page => (
              <Route
                key={page.path}
                path={page.path}
                component={page.component}
                exact={page.exact} />
            ))}
          </Switch>
        </DefaultLayout>
      </Suspense>
    </BrowserRouter>
  )
}

export default Router
