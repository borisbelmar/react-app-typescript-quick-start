import React, { ReactElement } from 'react'

interface DefaultLayoutProps {
  children?: ReactElement
}

const DefaultLayout = ({ children }: DefaultLayoutProps) => {
  return (
    <main>
      <nav>This is navigation</nav>
      {children}
      <footer>This is the footer</footer>
    </main>
  )
}

export default DefaultLayout
