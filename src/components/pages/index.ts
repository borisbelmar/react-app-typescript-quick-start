import React from 'react'

interface Page {
  path: string
  exact: boolean
  component: React.LazyExoticComponent<React.FC>
}

const pages: Page[] = [
  {
    path: '/',
    exact: true,
    component: React.lazy(() => import('./Home'))
  },
  {
    path: '/page',
    exact: true,
    component: React.lazy(() => import('./Page'))
  },
  {
    path: '/404',
    exact: false,
    component: React.lazy(() => import('./NotFound'))
  }
]

export default pages
